#!/usr/bin/python3

import os
from project import app
from bottle import debug, run


DEBUG = os.environ.get("DEBUG", True)
debug(DEBUG)
if __name__ == "__main__":
    port = int(os.environ.get("PORT", 8000))
    run(app, host="0.0.0.0", port=port, reloader=True)
