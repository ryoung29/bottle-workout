CREATE TABLE workout_types (id INTEGER PRIMARY KEY, name CHAR(50));
CREATE TABLE units (id INTEGER PRIMARY KEY, name CHAR(50));
CREATE TABLE workouts (
                       id INTEGER PRIMARY KEY,
                       workout_type INTEGER,
                       xdate CHAR(20) NOT NULL,
                       qty INTEGER,
                       amt INTEGER,
                       units INTEGER,
                       FOREIGN KEY(workout_type) REFERENCES workout_types(id) ON UPDATE CASCADE,
                       FOREIGN KEY(units) REFERENCES units(id) ON UPDATE CASCADE
                      );
INSERT INTO units (name) VALUES ('miles'),
                                ('reps'),
                                ('sets'),
                                ('minutes'),
                                ('laps');
INSERT INTO workout_types (name) VALUES ('abs'),
                                        ('back'),
                                        ('biceps'),
                                        ('chest'),
                                        ('triceps'),
                                        ('run'),
                                        ('cardio'),
                                        ('quads'),
                                        ('hamstrings'),
                                        ('swim'),
                                        ('walk'),
                                        ('legs');
