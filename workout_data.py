#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sqlite3


DB_PATH = 'workout.db'

create_types = """
INSERT INTO workout_types VALUES (1, 'abs'), (2, 'back'), (3, 'biceps'), (4, 'chest'),
    (5, 'triceps'), (6, 'run'), (7, 'cardio');
"""

create_units = """
INSERT INTO units VALUES (1, 'miles'), (2, 'reps'), (3, 'sets'), (4, 'minutes');
"""

create_units_table = """
CREATE TABLE units (id INTEGER PRIMARY KEY, name CHAR(50));
"""

create_types_table = """
CREATE TABLE workout_types (id INTEGER PRIMARY KEY, name CHAR(50));
"""

create_workout_table = """
CREATE TABLE workouts (
                       id INTEGER PRIMARY KEY,
                       workout_type INTEGER,
                       xdate CHAR(20) NOT NULL,
                       qty INTEGER,
                       amt INTEGER,
                       units INTEGER,
                       FOREIGN KEY(workout_type) REFERENCES workout_types(id) ON UPDATE CASCADE,
                       FOREIGN KEY(units) REFERENCES units(id) ON UPDATE CASCADE
                      );
              """

db = sqlite3.connect(DB_PATH)
db.execute(create_types_table)
db.execute(create_units_table)
db.execute(create_workout_table)
db.execute(create_types)
db.execute(create_units)
db.commit()
