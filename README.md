# My Workout App

I am using this to get an introduction to the Bottle microframework. I
am employing a _Model-View-Controller_ design pattern from [This GitHub Project](https://github.com/salimane/bottle-mvc/).

# Key Features

- sqlite3
- bootstrap.js
- bottle

# Installation and Setup

Create a virtual environment.

``` bash
virtualenv workout-app && source workout-app/bin/activate
```

Install requirements.

``` bash
pip install -r requirements.txt
```

Initiate the database with either:

``` bash
sqlite3 workout.db < create_schema.sql
```

or:

``` bash
python3 workout_data.py
```

# Todo

- ~~Dashboard with summary of previous 30 days of activity~~
