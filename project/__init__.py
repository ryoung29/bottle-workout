#!/usr/bin/env python
# -*- coding: utf-8 -*-
__version__ = '0.2'
from bottle import Bottle, TEMPLATE_PATH

app = Bottle()
TEMPLATE_PATH.append("./project/views")

from project.controllers import *
