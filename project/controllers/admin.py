#!/usr/bin/env python
# -*- coding: utf-8 -*-

from project import app
from bottle import redirect, request, template


@app.route('/admin')
def index():
    return template('admin/home')


@app.route('/admin/types/delete')
def types():
    if request.method == "GET":
        from project.models.Types import Types
        t = Types()
        try:
            types = t.get_types()
            output = template('admin/types/delete', types=types)
        except Exception as e:
            output = str(e)
        return output
    return ""


@app.post('/admin/types/delete')
def remove_types():
    if request.method == 'POST':
        from project.models.Types import Types

        t = Types()
        xtype = request.forms.get('type')
        t.delete(xtype)
        redirect('/admin')
    else:
        redirect('/admin')


@app.route('/admin/units/delete')
def units():
    if request.method == "GET":
        from project.models.Units import Units
        u = Units()
        try:
            units = u.get_units()
            output = template('admin/units/delete', units=units)
        except Exception as e:
            output = str(e)
        return output
    return ""


@app.post('/admin/units/delete')
def remove_units():
    if request.method == 'POST':
        from project.models.Units import Units

        u = Units()
        units = request.forms.get('units')
        u.delete(units)
        redirect('/admin')
    else:
        redirect('/admin')


@app.route('/admin/units/new')
def new_units():
    if request.method == "GET":
        return template('admin/units/new')


@app.post('/admin/units/new')
def add_units():
    if request.method == 'POST':
        from project.models.Units import Units

        u = Units()
        units = request.forms.get('units')
        u.add_unit(units)
        redirect('/admin')
    else:
        redirect('/admin')


@app.route('/admin/types/new')
def new_type():
    if request.method == "GET":
        return template('admin/types/new')


@app.post('/admin/types/new')
def add_type():
    if request.method == 'POST':
        from project.models.Types import Types

        t = Types()
        xtype = request.forms.get('type')
        t.add_type(xtype)
        redirect('/admin/types/new')
    else:
        redirect('/admin')
