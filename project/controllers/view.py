#!/usr/bin/env python
# -*- coding: utf-8 -*-

from project import app
from bottle import request, template

@app.route('/view/<pager:int>')
def view(pager=0):
    try:
        from project.models.Workouts import Workouts
        w = Workouts()
        data = w.get_workouts(pager)
        output = template('main/view', rows=data, pager=pager)
    except Exception as e:
        output = str(e)
    return output

