#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd

from bokeh.palettes import Category10
from bokeh.models.mappers import CategoricalColorMapper
from bokeh.embed import components
from bokeh.models import DatetimeTickFormatter
from bokeh.plotting import figure
from bokeh.resources import CDN
from bottle import template
from project import app

import sqlite3

from math import radians

DB_PATH = 'workout.db'


@app.route('/charts')
def dashboard():
    sql = """
    select w.amt, w.qty, w.xdate, wt.name as workout_type
    from workouts w
    join workout_types wt on w.workout_type = wt.id
    where w.xdate > date('now', '-30 days');
    """

    # Get Data
    with sqlite3.connect(DB_PATH) as conn:
        data = pd.read_sql_query(sql, conn)

    wtypes = list(data.workout_type.unique())
    palette = CategoricalColorMapper(palette=Category10[10], factors=wtypes)

    # Define chart
    f = figure(x_axis_type='datetime')

    # Colorize each workout type, size by quantity
    for wtype, color in zip(wtypes, palette.palette):
        f.circle(x=pd.to_datetime(data['xdate'][data["workout_type"] == wtype]),
                 y=data['amt'][data["workout_type"] == wtype],
                 fill_alpha=0.2,
                 size=data['qty'] * 4,
                 color=color,
                 legend=wtype)

    # Stylize chart
    f.title.text = "Size = quantity"
    f.title.align = "center"
    f.xaxis.axis_label = "Date"
    f.xaxis.major_label_orientation = radians(90)
    f.xaxis.formatter = DatetimeTickFormatter(
        seconds=["%Y-%m-%d-%H-%M-%S"],
        minsec=["%Y-%m-%d-%H-%M-%S"],
        minutes=["%Y-%m-%d-%H-%M-%S"],
        hourmin=["%Y-%m-%d-%H-%M-%S"],
        hours=["%Y-%m-%d-%H-%M-%S"],
        days=["%Y-%m-%d"],
        months=["%Y-%m-%d"],
        years=["%Y-%m-%d"],
    )

    f.yaxis.axis_label = "Amount"

# Serve chart to template
    js, div = components(f)
    cdn_js = CDN.js_files[0]
    try:
        cdn_css = CDN.css_files[0]
    except IndexError:
        cdn_css = None
    return template('charts/home', js=js, div=div,
                    cdn_js=cdn_js, cdn_css=cdn_css)
