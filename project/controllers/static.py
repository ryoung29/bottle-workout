#!/usr/bin/env python
# -*- coding: utf-8 -*-

from project import app
from bottle import static_file


@app.route('/:file#(favicon.ico|humans.txt)#')
def favicon(file):
    """Find favicon"""
    return static_file(file, root="project/static/misc")

@app.route('/:file#(workout.js)#')
def myjs(file):
    """Find custom js"""
    return static_file(file, root="project/static/myjs")

@app.route('/:path#(images|css|js|fonts)\/.+#')
def server_static(path):
    """Find static data"""
    return static_file(path, root="project/static/bootstrap")
