#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import path
import glob


__all__ = [path.basename(f)[:-3] for f in glob.glob(
                         path.dirname(__file__) + "/*.py")]
