#!/usr/bin/env python
# -*- coding: utf-8 -*-

from project import app
from bottle import redirect, request, template

@app.post('/update/<id:int>')
def get_workout(id):
    if request.method == 'POST':
        from project.models.Workouts import Workouts
        w = Workouts()
        xtype = request.forms.get('type')
        xdate = request.forms.get('date')
        qty = request.forms.get('qty')
        amt = request.forms.get('amt')
        units = request.forms.get('units')
        input_data = (xdate, int(xtype), int(qty), int(amt), int(units))
        update_id = w.update_workout(id, input_data)
        redirect('/view/0')


@app.route('/update/<id:int>')
def get_workout(id):
    if request.method == 'GET':
        from project.models.Workouts import Workouts
        from project.models.Units import Units
        from project.models.Types import Types
        u = Units()
        t = Types()
        try:
            wo = Workouts()
            units = u.get_units()
            types = t.get_types()
            xtype, xdate, qty, amt, unit = wo.get_workout(id)
            output = template(
                'main/update',
                units=units,
                types=types,
                id=id,
                itype=xtype,
                idate=xdate,
                iqty=qty,
                iamt=amt,
                iunits=unit
            )
        except Exception as e:
            output = str(e)
        return output
