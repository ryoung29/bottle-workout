#!/usr/bin/env python
# -*- coding: utf-8 -*-

from project import app
from bottle import redirect, request, template

@app.route('/')
def index():
    return template('main/home')

@app.route('/new')
def new():
    if request.method == "GET":
        from project.models.Units import Units
        from project.models.Types import Types
        u = Units()
        t = Types()
        try:
            units = u.get_units()
            types = t.get_types()
            output = template('main/new', units=units, types=types)
        except Exception as e:
            output = str(e)
        return output
    return ""

@app.post('/new')
def add_new():
    if request.method == 'POST':
        from project.models.Workouts import Workouts

        w = Workouts()
        xtype = request.forms.get('type')
        xdate = request.forms.get('date')
        qty = request.forms.get('qty')
        amt = request.forms.get('amt')
        units = request.forms.get('units')
        input_data = (xdate, int(xtype), int(qty), int(amt), int(units))
        new_id = w.new_workout(input_data)
        redirect('/view/0')
    else:
        redirect('/')
