#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3

DB_PATH = 'workout.db'


class Units(object):
    @staticmethod
    def get_units():
        db = sqlite3.connect(DB_PATH)
        c = db.cursor()
        c.execute("SELECT * FROM units ORDER BY name")
        units = c.fetchall()
        c.close()
        return units

    @staticmethod
    def delete(units):
        db = sqlite3.connect(DB_PATH)
        c = db.cursor()
        ok = c.execute("DELETE from units where id = ?;", (units,))
        db.commit()
        c.close()

    @staticmethod
    def add_unit(unit):
        db = sqlite3.connect(DB_PATH)
        c = db.cursor()
        ok = c.execute("INSERT INTO units (name) VALUES (?)", (unit,))
        db.commit()
        c.close()
        return ok
