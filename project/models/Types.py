#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3

DB_PATH = 'workout.db'


class Types(object):
    @staticmethod
    def get_types():
        db = sqlite3.connect(DB_PATH)
        c = db.cursor()
        c.execute("SELECT * FROM workout_types ORDER BY name;")
        types = c.fetchall()
        c.close()
        return types

    @staticmethod
    def delete(xtype):
        db = sqlite3.connect(DB_PATH)
        c = db.cursor()
        ok = c.execute("DELETE from workout_types where id = ?;", (xtype,))
        db.commit()
        c.close()

    @staticmethod
    def add_type(xtype):
        db = sqlite3.connect(DB_PATH)
        c = db.cursor()
        ok = c.execute("INSERT INTO workout_types (name) VALUES (?)", (xtype,))
        db.commit()
        c.close()
        return ok
