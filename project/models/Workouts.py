#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3


DB_PATH = 'workout.db'
CONNECTION = sqlite3.connect(DB_PATH)


class Workouts(object):
    @staticmethod
    def get_workouts(pager):
        end_date = pager * 10
        start_date = end_date + 10
        db = sqlite3.connect(DB_PATH)
        c = db.cursor()
        statement = """
        SELECT w.name, wo.xdate, wo.qty, wo.amt, u.name, wo.id
        FROM workouts wo
        JOIN workout_types w ON w.id=wo.workout_type
        JOIN units u ON u.id = wo.units
        WHERE date(xdate) BETWEEN strftime('%Y-%m-%d', 'now', '-"""
        statement += str(start_date) + " days') and strftime('%Y-%m-%d', 'now', '-"
        statement += str(end_date) + " days') ORDER BY wo.xdate DESC"
        c.execute(statement)
        data = c.fetchall()
        c.close()
        return data

    @staticmethod
    def new_workout(data):
        db = CONNECTION
        c = db.cursor()
        statement = "INSERT INTO workouts (xdate, workout_type, qty, amt, units) VALUES (?, ?, ?, ?, ?)"
        c.execute(statement, data)
        new_id = c.lastrowid

        db.commit()
        c.close()
        return new_id

    @staticmethod
    def update_workout(id, data):
        db = CONNECTION
        c = db.cursor()
        xdate = data[0]
        workout_type = data[1]
        qty = data[2]
        amt = data[3]
        units = data[4]
        statement = f"""UPDATE workouts
        set xdate = '{xdate}',
        workout_type = {workout_type},
        qty = {qty},
        amt = {amt},
        units = {units}
        WHERE id = {id}"""
        c.execute(statement)

        db.commit()
        c.close()
        return id

    @staticmethod
    def get_workout(id):
        db = CONNECTION
        c = db.cursor()
        statement = f"SELECT * FROM workouts WHERE id = {id}"
        c.execute(statement)
        _, *data = c.fetchone()
        c.close()
        return data
