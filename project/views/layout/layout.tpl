<!doctype html5>
     <head>
         <link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
         <title>{{ title }}</title>
         <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
     </head>
     <body class="container">
         <nav class="navbar navbar-default">
             <div class="container-fluid">
                 <a class="btn btn-info navbar-btn" href="/">Home</a>
                 <a class="btn btn-info navbar-btn" href="/admin">Admin</a>
             </div>
         </nav>
         <div class="page">
             %include
        </div>
        <footer class="footer"><div class="navbar navbar-default navbar-fixed-bottom">
        <center class="text-muted">RY Workouts</center>
        </div></footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    </body>
</html>
