<h1>Dashboard</h1>

<div class="panel">
    {{ !div }}
    {{ !js }}
</div>

<p>Chart shows last <em>30 days</em> of activity</p>
%rebase("layout/charts", title="Dashboard")
