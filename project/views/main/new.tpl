<h1>New Workout</h1>
<div class="btn-group btn-group-justified" role="toolbar" aria-label="...">
    <a href="../" class="btn btn-primary">Home</a>
    <a href="../view/0" class="btn btn-primary">View</a>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Add a workout</div>
<form action="/new" method="POST">
<div class="form-group">
    Date:<input class="form-control" name="date" type="date">
    Excercise type:<select onchange="typeChange()" class="form-control" name="type">
    %for xtype in types:
        <option value="{{xtype[0]}}">{{xtype[1]}}</option>
    %end
    </select>
    Quantity:<input class="form-control" name="qty" type="number" >
    Amount:<input class="form-control" name="amt" type="number" >
    Units:<select class="form-control" name="units">
    %for unit in units:
        <option value="{{unit[0]}}">{{unit[1]}}</option>
    %end
    </select>
    <br>
    <input class="btn btn-primary" type="submit" value="save">
</div>
</form>
</div>
<script src="/workout.js"></script>

%rebase("layout/layout", title="New")
