<h1>View Workouts</h1>
<div class="btn-group btn-group-justified" role="toolbar" aria-label="...">
    <a href="../" class="btn btn-primary">Home</a>
    <a href="../new" class="btn btn-primary">New</a>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
    %if len(rows) > 0:
        <a href="./{{pager + 1}}" class="btn btn-default">
            <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
            Earlier
        </a>
    %else:
        <span class="label label-default">Earlier</span>
    %end
    %if pager > 0:
        <a href="./{{pager - 1}}" class="btn btn-default">
            <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
            Later
        </a>
    %else:
        <span class="label label-default">Later</span>
    %end
    </div>
    <table class="table table-striped table-condensed">
        <tr><th>Date</th><th>Type</th><th>QTY</th><th>AMT</th><th>Units</th></tr>
    %for row in rows:
        <tr><td><a href="/update/{{row[5]}}">{{row[1]}}</a></td><td>{{row[0]}}</td><td>{{row[2]}}</td><td>{{row[3]}}</td><td>{{row[4]}}</td></tr>
    %end
    </table>
</div>

% rebase("layout/layout", title="View")
