<div class="jumbotron">
    <h1>Which One? <small>Workout types or Units?</small></h1>
    <ul class="pager">
        <li><a class="btn btn-lg" href="/charts">Dashboard »</li></a>
    </ul>
</div>

<div class="container-fluid">
    <h2>Workout types</h2>
    <ul class="pager">
        <li><a href="./admin/types/new">Add</a></li>
        <li><a href="./admin/types/delete">Delete</a></li>
    </ul>
    <h2>Units</h2>
        <ul class="pager">
            <li><a href="./admin/units/new">Add</a></li>
            <li><a href="./admin/units/delete">Delete</a></li>
        </ul>
</div>

%rebase("layout/layout", title="Admin home")
