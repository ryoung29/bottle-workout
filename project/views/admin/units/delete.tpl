<h1>Delete Workout Type</h1>
<div class="btn-group btn-group-justified" role="toolbar" aria-label="...">
    <a href="/admin" class="btn btn-primary">Admin Home</a>
    <a href="./new" class="btn btn-primary">Add New</a>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Delete a unit of measure</div>
<form action="/admin/units/delete" method="POST">
<div class="form-group">
    Units:<select class="form-control" name="units">
    %for unit in units:
        <option value="{{unit[0]}}">{{unit[1]}}</option>
    %end
    </select>
    <br>
    <input class="btn btn-primary" type="submit" value="delete">
</div>
</form>
</div>

%rebase("layout/layout", title="Delete Unit of Measure")
