<h1>Delete Workout Type</h1>
<div class="btn-group btn-group-justified" role="toolbar" aria-label="...">
    <a href="/admin" class="btn btn-primary">Admin Home</a>
    <a href="./new" class="btn btn-primary">Add New</a>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Delete a workout type</div>
<form action="/admin/types/delete" method="POST">
<div class="form-group">
    Exercise type:<select class="form-control" name="type">
    %for xtype in types:
        <option value="{{xtype[0]}}">{{xtype[1]}}</option>
    %end
    </select>
    <br>
    <input class="btn btn-primary" type="submit" value="delete">
</div>
</form>
</div>

%rebase("layout/layout", title="Delete Exercise Type")
