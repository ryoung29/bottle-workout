<h1>New Workout Type</h1>
<div class="btn-group btn-group-justified" role="toolbar" aria-label="...">
    <a href="/admin" class="btn btn-primary">Admin Home</a>
    <a href="./delete" class="btn btn-primary">Delete</a>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Add a workout type</div>
<form action="/admin/types/new" method="POST">
<div class="form-group">
    Exercise Type:<input class="form-control" name="type" type="text">
    <br>
    <input class="btn btn-primary" type="submit" value="save">
</div>
</form>
</div>

%rebase("layout/layout", title="Add Exercise Type")
