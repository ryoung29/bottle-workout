// If workout type is run or walk, set quantity to 1
function typeChange(){
	switch (document.getElementsByName("type")[0].value) {
		case "6":   // run
		case "12":  // walk
		case "14":  // bike
			document.getElementsByName("qty")[0].value = "1";
			document.getElementsByName("units")[0].value = "1";  // miles
			document.getElementsByName("amt")[0].focus();
			break;
        case "7":   // cardio
			document.getElementsByName("qty")[0].value = "1";
			document.getElementsByName("units")[0].value = "4";  // minutes
			document.getElementsByName("qty")[0].focus();
			break;
        case "11":  // swim
			document.getElementsByName("qty")[0].value = "1";
			document.getElementsByName("units")[0].value = "5";  // laps
			document.getElementsByName("amt")[0].focus();
            break;
		default:
			document.getElementsByName("qty")[0].value = "";
			document.getElementsByName("units")[0].value = "2";  // reps
			document.getElementsByName("qty")[0].focus();
	}
			
    //if (document.getElementsByName("type")[0].value == 6 || document.getElementsByName("type")[0].value == 11){
        //document.getElementsByName("qty")[0].value = "1";
        //document.getElementsByName("qty")[0].readOnly = true;
    //}
    //else{
        //document.getElementsByName("qty")[0].value = "";
        //document.getElementsByName("qty")[0].readOnly = false;
    //}
}
